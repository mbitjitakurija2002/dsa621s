import ballerina/http;
User [] allusers=[];

listener http:Listener ep0 = new (8080, config = {host: "localhost"});

service /vle/api/v1 on ep0 {
    resource function get users() returns User[]|http:Response {
    return allusers;
    
    }
    resource function post users(@http:Payload User payload) returns CreatedInlineResponse201|http:Response {
    allusers.push(payload);
    InlineResponse201 y={userid: "1"};
    CreatedInlineResponse201 r={body:y};
    return r;
   
    }
    resource function get users/[string  username]() returns User|http:Response {
      User thatUser={};
           foreach var item in allusers {
        if(item.username==username){

         thatUser=item;
        }
        
     }
     return thatUser;
  
    }
    resource function put users/[string  username](@http:Payload User payload) returns User|http:Response {
     User thatUser={};
           foreach var item in allusers {
        if(item.username==username){
            item.removeAll();
            allusers.push(payload);   
        }
        
     }
     return thatUser;
  
    }
    resource function delete users/[string  username]() returns http:NoContent|http:Response {
    User thatUser={};
           foreach var item in allusers {
        if(item.username==username){
            thatUser=item;
            thatUser.removeAll();
        }
        
     }
     return http:NO_CONTENT;
  
    }
}